/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Granberg;

import com.Granberg.model.*;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlo
 */
public class MySQLDBAccessTest {
    private IDBAccess dbAccess;
    public MySQLDBAccessTest() {
        dbAccess = new MySQLDBAccess();
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findMarket method, of class MySQLDBAccess.
     */
    @Test
    public void testFindMarket() {
       List<Market> markets = dbAccess.findMarket();
       for(Market market:markets){
           System.out.println(market.getMarketName());
       }
    }
    
    @Test
    public void testFindInstrument(){
        List<Instrument> instruments = dbAccess.findInstrument();
        for(Instrument instrument:instruments){
            System.out.println(instrument.getInstrumentName());
        }
    }
    
    @Test
    public void testFindInstrumentType(){
        List<InstrumentType> instrumentTypes = dbAccess.findInstrumentType();
        for(InstrumentType instrumentType:instrumentTypes){
            System.out.println(instrumentType.getInstrumentTypeName());
        }
    }
    
    @Test
    public void testFindOrderType(){
    
        List<OrderType> orderTypes = dbAccess.findOrderType();
        for(OrderType orderType:orderTypes){
            System.out.println(orderType.getOrderTypeName());
        }
    //assertNotNull(orderTypes.get(0));
    
    }
}
