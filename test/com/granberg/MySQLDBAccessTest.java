/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg;

import com.granberg.model.OrderType;
import com.granberg.model.Instrument;
import com.granberg.model.Market;
import com.granberg.model.InstrumentType;
import com.granberg.MySQLDBAccess;
import com.granberg.IDBAccess;
import com.granberg.model.Contract;
import com.granberg.model.Periodicity;
import com.granberg.model.Position;
import com.granberg.model.Ticket;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Carlo
 */
public class MySQLDBAccessTest {
    private IDBAccess dbAccess;
    public MySQLDBAccessTest() {
        dbAccess = new MySQLDBAccess();
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of findMarket method, of class MySQLDBAccess.
     */
    @Test
    public void testFindMarket() {
       List<Market> markets = dbAccess.findMarket();
       for(Market market:markets){
           System.out.println(market.getMarketName());
       }
    }
    
    @Test
    public void testFindInstrument(){
        List<Instrument> instruments = dbAccess.findInstrument();
        for(Instrument instrument:instruments){
            System.out.println(instrument.getInstrumentName());
        }
    }
    
    @Test
    public void testFindInstrumentType(){
        List<InstrumentType> instrumentTypes = dbAccess.findInstrumentType();
        for(InstrumentType instrumentType:instrumentTypes){
            System.out.println(instrumentType.getInstrumentTypeName());
        }
    }
    
    @Test
    public void testFindOrderType(){
    
        List<OrderType> orderTypes = dbAccess.findOrderType();
        for(OrderType orderType:orderTypes){
            System.out.println(orderType.getOrderTypeName());
        }
    }
    
    @Test
    public void testFindContract(){
    
        List<Contract> contracts = dbAccess.findContracts();
        for(Contract contract:contracts){
            System.out.println(contract.getInstrumentId());
        }
    }
    
    @Test
    public void testFindTicket(){
        List<Ticket> tickets = dbAccess.findTickets();
        for (Ticket ticket : tickets) {
            System.out.println(ticket.getTicketId());
        }
    }
    
    @Test
    public void testFindPosition(){
        List<Position> positions = dbAccess.findPositions();
        for (Position position : positions) {
            System.out.println(position.getPositionName());
        }
    }
    
    @Test
    public void testFindPeriodicity(){
        List<Periodicity> periodicitys = dbAccess.findPeriodicity();
        for (Periodicity periodicity : periodicitys) {
            System.out.println(periodicity.getPeriodicityName());
        }
    }
}
