/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Granberg;

import com.Granberg.model.*;
import java.util.List;

/**
 *
 * @author Carlo
 */
public interface IDBAccess {
    List<Market> findMarket();
    List<Instrument> findInstrument();
    List<InstrumentType> findInstrumentType();
    List<OrderType> findOrderType();
}
