/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Granberg.controller;

import com.Granberg.controller.exceptions.NonexistentEntityException;
import com.Granberg.model.InstrumentType;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class InstrumentTypeJpaController implements Serializable {

    public InstrumentTypeJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(InstrumentType instrumentType) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(instrumentType);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(InstrumentType instrumentType) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            instrumentType = em.merge(instrumentType);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = instrumentType.getInstrumentTypeId();
                if (findInstrumentType(id) == null) {
                    throw new NonexistentEntityException("The instrumentType with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            InstrumentType instrumentType;
            try {
                instrumentType = em.getReference(InstrumentType.class, id);
                instrumentType.getInstrumentTypeId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The instrumentType with id " + id + " no longer exists.", enfe);
            }
            em.remove(instrumentType);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<InstrumentType> findInstrumentTypeEntities() {
        return findInstrumentTypeEntities(true, -1, -1);
    }

    public List<InstrumentType> findInstrumentTypeEntities(int maxResults, int firstResult) {
        return findInstrumentTypeEntities(false, maxResults, firstResult);
    }

    private List<InstrumentType> findInstrumentTypeEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(InstrumentType.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public InstrumentType findInstrumentType(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(InstrumentType.class, id);
        } finally {
            em.close();
        }
    }

    public int getInstrumentTypeCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<InstrumentType> rt = cq.from(InstrumentType.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
