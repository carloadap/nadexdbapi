/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Granberg.controller;

import com.Granberg.controller.exceptions.NonexistentEntityException;
import com.Granberg.model.Market;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class MarketJpaController implements Serializable {

    public MarketJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Market market) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(market);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Market market) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            market = em.merge(market);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = market.getMarketId();
                if (findMarket(id) == null) {
                    throw new NonexistentEntityException("The market with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Market market;
            try {
                market = em.getReference(Market.class, id);
                market.getMarketId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The market with id " + id + " no longer exists.", enfe);
            }
            em.remove(market);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Market> findMarketEntities() {
        return findMarketEntities(true, -1, -1);
    }

    public List<Market> findMarketEntities(int maxResults, int firstResult) {
        return findMarketEntities(false, maxResults, firstResult);
    }

    private List<Market> findMarketEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Market.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Market findMarket(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Market.class, id);
        } finally {
            em.close();
        }
    }

    public int getMarketCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Market> rt = cq.from(Market.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
