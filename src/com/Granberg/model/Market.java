/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "Market")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Market.findAll", query = "SELECT m FROM Market m"),
    @NamedQuery(name = "Market.findByMarketId", query = "SELECT m FROM Market m WHERE m.marketId = :marketId"),
    @NamedQuery(name = "Market.findByMarketName", query = "SELECT m FROM Market m WHERE m.marketName = :marketName"),
    @NamedQuery(name = "Market.findByMarketType", query = "SELECT m FROM Market m WHERE m.marketType = :marketType")})
public class Market implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "market_id")
    private Integer marketId;
    @Basic(optional = false)
    @Column(name = "market_name")
    private String marketName;
    @Basic(optional = false)
    @Column(name = "market_type")
    private String marketType;

    public Market() {
    }

    public Market(Integer marketId) {
        this.marketId = marketId;
    }

    public Market(Integer marketId, String marketName, String marketType) {
        this.marketId = marketId;
        this.marketName = marketName;
        this.marketType = marketType;
    }

    public Integer getMarketId() {
        return marketId;
    }

    public void setMarketId(Integer marketId) {
        this.marketId = marketId;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getMarketType() {
        return marketType;
    }

    public void setMarketType(String marketType) {
        this.marketType = marketType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (marketId != null ? marketId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Market)) {
            return false;
        }
        Market other = (Market) object;
        if ((this.marketId == null && other.marketId != null) || (this.marketId != null && !this.marketId.equals(other.marketId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.Granberg.model.Market[ marketId=" + marketId + " ]";
    }
    
}
