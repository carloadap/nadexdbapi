/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "Instrument")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Instrument.findAll", query = "SELECT i FROM Instrument i"),
    @NamedQuery(name = "Instrument.findByInstrumentId", query = "SELECT i FROM Instrument i WHERE i.instrumentId = :instrumentId"),
    @NamedQuery(name = "Instrument.findByInstrumentName", query = "SELECT i FROM Instrument i WHERE i.instrumentName = :instrumentName"),
    @NamedQuery(name = "Instrument.findByInstrumentTypeId", query = "SELECT i FROM Instrument i WHERE i.instrumentTypeId = :instrumentTypeId")})
public class Instrument implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "instrument_id")
    private Integer instrumentId;
    @Basic(optional = false)
    @Column(name = "instrument_name")
    private String instrumentName;
    @Basic(optional = false)
    @Column(name = "instrument_type_id")
    private int instrumentTypeId;

    public Instrument() {
    }

    public Instrument(Integer instrumentId) {
        this.instrumentId = instrumentId;
    }

    public Instrument(Integer instrumentId, String instrumentName, int instrumentTypeId) {
        this.instrumentId = instrumentId;
        this.instrumentName = instrumentName;
        this.instrumentTypeId = instrumentTypeId;
    }

    public Integer getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(Integer instrumentId) {
        this.instrumentId = instrumentId;
    }

    public String getInstrumentName() {
        return instrumentName;
    }

    public void setInstrumentName(String instrumentName) {
        this.instrumentName = instrumentName;
    }

    public int getInstrumentTypeId() {
        return instrumentTypeId;
    }

    public void setInstrumentTypeId(int instrumentTypeId) {
        this.instrumentTypeId = instrumentTypeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instrumentId != null ? instrumentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Instrument)) {
            return false;
        }
        Instrument other = (Instrument) object;
        if ((this.instrumentId == null && other.instrumentId != null) || (this.instrumentId != null && !this.instrumentId.equals(other.instrumentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.Granberg.model.Instrument[ instrumentId=" + instrumentId + " ]";
    }
    
}
