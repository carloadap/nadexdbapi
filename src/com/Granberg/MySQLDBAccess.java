/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Granberg;

import com.Granberg.controller.InstrumentJpaController;
import com.Granberg.controller.InstrumentTypeJpaController;
import com.Granberg.controller.MarketJpaController;
import com.Granberg.controller.OrderTypeJpaController;
import com.Granberg.model.Instrument;
import com.Granberg.model.InstrumentType;
import com.Granberg.model.Market;
import com.Granberg.model.OrderType;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Carlo
 */
public class MySQLDBAccess implements IDBAccess {
    private EntityManagerFactory emf;
    public MySQLDBAccess(){
        emf = Persistence.createEntityManagerFactory("NadexDBAPIPU");
    }
    @Override
    public List<Market> findMarket() {
        
        MarketJpaController marketController = new MarketJpaController(emf);
        return marketController.findMarketEntities();
        
    }

    @Override
    public List<Instrument> findInstrument() {
        InstrumentJpaController instrumentController = new InstrumentJpaController(emf);
        return instrumentController.findInstrumentEntities();
    }

    @Override
    public List<InstrumentType> findInstrumentType() {
        InstrumentTypeJpaController instrumentTypeController = new InstrumentTypeJpaController(emf);
        return instrumentTypeController.findInstrumentTypeEntities();
    }

    @Override
    public List<OrderType> findOrderType() {
        OrderTypeJpaController orderTypeController = new OrderTypeJpaController(emf);
        return orderTypeController.findOrderTypeEntities();
    }
    
}
