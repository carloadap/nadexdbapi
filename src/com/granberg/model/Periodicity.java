/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "Periodicity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Periodicity.findAll", query = "SELECT p FROM Periodicity p"),
    @NamedQuery(name = "Periodicity.findByPeriodicityId", query = "SELECT p FROM Periodicity p WHERE p.periodicityId = :periodicityId"),
    @NamedQuery(name = "Periodicity.findByPeriodicityName", query = "SELECT p FROM Periodicity p WHERE p.periodicityName = :periodicityName")})
public class Periodicity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "periodicity_id")
    private Integer periodicityId;
    @Basic(optional = false)
    @Column(name = "periodicity_name")
    private String periodicityName;

    public Periodicity() {
    }

    public Periodicity(Integer periodicityId) {
        this.periodicityId = periodicityId;
    }

    public Periodicity(Integer periodicityId, String periodicityName) {
        this.periodicityId = periodicityId;
        this.periodicityName = periodicityName;
    }

    public Integer getPeriodicityId() {
        return periodicityId;
    }

    public void setPeriodicityId(Integer periodicityId) {
        this.periodicityId = periodicityId;
    }

    public String getPeriodicityName() {
        return periodicityName;
    }

    public void setPeriodicityName(String periodicityName) {
        this.periodicityName = periodicityName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (periodicityId != null ? periodicityId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Periodicity)) {
            return false;
        }
        Periodicity other = (Periodicity) object;
        if ((this.periodicityId == null && other.periodicityId != null) || (this.periodicityId != null && !this.periodicityId.equals(other.periodicityId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.Periodicity[ periodicityId=" + periodicityId + " ]";
    }
    
}
