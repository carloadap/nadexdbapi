/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "Contract")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contract.findAll", query = "SELECT c FROM Contract c"),
    @NamedQuery(name = "Contract.findByContractId", query = "SELECT c FROM Contract c WHERE c.contractId = :contractId"),
    @NamedQuery(name = "Contract.findByInstrumentId", query = "SELECT c FROM Contract c WHERE c.instrumentId = :instrumentId"),
    @NamedQuery(name = "Contract.findByOrderTypeId", query = "SELECT c FROM Contract c WHERE c.orderTypeId = :orderTypeId"),
    @NamedQuery(name = "Contract.findByCeiling", query = "SELECT c FROM Contract c WHERE c.ceiling = :ceiling"),
    @NamedQuery(name = "Contract.findByFloor", query = "SELECT c FROM Contract c WHERE c.floor = :floor"),
    @NamedQuery(name = "Contract.findByExpirationDate", query = "SELECT c FROM Contract c WHERE c.expirationDate = :expirationDate"),
    @NamedQuery(name = "Contract.findByActive", query = "SELECT c FROM Contract c WHERE c.active = :active"),
    @NamedQuery(name = "Contract.findByOptionTypeId", query = "SELECT c FROM Contract c WHERE c.optionTypeId = :optionTypeId"),
    @NamedQuery(name = "Contract.findByUpdatedOn", query = "SELECT c FROM Contract c WHERE c.updatedOn = :updatedOn"),
    @NamedQuery(name = "Contract.findByPeriodicityId", query = "SELECT c FROM Contract c WHERE c.periodicityId = :periodicityId")})
public class Contract implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "contract_id")
    private Integer contractId;
    @Basic(optional = false)
    @Column(name = "instrument_id")
    private int instrumentId;
    @Basic(optional = false)
    @Column(name = "order_type_id")
    private int orderTypeId;
    @Basic(optional = false)
    @Column(name = "ceiling")
    private long ceiling;
    @Basic(optional = false)
    @Column(name = "floor")
    private long floor;
    @Basic(optional = false)
    @Column(name = "expiration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;
    @Basic(optional = false)
    @Column(name = "active")
    private String active;
    @Basic(optional = false)
    @Column(name = "option_type_id")
    private int optionTypeId;
    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;
    @Column(name = "periodicity_id")
    private Integer periodicityId;

    public Contract() {
    }

    public Contract(Integer contractId) {
        this.contractId = contractId;
    }

    public Contract(Integer contractId, int instrumentId, int orderTypeId, long ceiling, long floor, Date expirationDate, String active, int optionTypeId) {
        this.contractId = contractId;
        this.instrumentId = instrumentId;
        this.orderTypeId = orderTypeId;
        this.ceiling = ceiling;
        this.floor = floor;
        this.expirationDate = expirationDate;
        this.active = active;
        this.optionTypeId = optionTypeId;
    }

    public Integer getContractId() {
        return contractId;
    }

    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    public int getInstrumentId() {
        return instrumentId;
    }

    public void setInstrumentId(int instrumentId) {
        this.instrumentId = instrumentId;
    }

    public int getOrderTypeId() {
        return orderTypeId;
    }

    public void setOrderTypeId(int orderTypeId) {
        this.orderTypeId = orderTypeId;
    }

    public long getCeiling() {
        return ceiling;
    }

    public void setCeiling(long ceiling) {
        this.ceiling = ceiling;
    }

    public long getFloor() {
        return floor;
    }

    public void setFloor(long floor) {
        this.floor = floor;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public int getOptionTypeId() {
        return optionTypeId;
    }

    public void setOptionTypeId(int optionTypeId) {
        this.optionTypeId = optionTypeId;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Integer getPeriodicityId() {
        return periodicityId;
    }

    public void setPeriodicityId(Integer periodicityId) {
        this.periodicityId = periodicityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (contractId != null ? contractId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contract)) {
            return false;
        }
        Contract other = (Contract) object;
        if ((this.contractId == null && other.contractId != null) || (this.contractId != null && !this.contractId.equals(other.contractId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.granberg.model.Contract[ contractId=" + contractId + " ]";
    }
    
}
