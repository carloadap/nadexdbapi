/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Carlo
 */
@Entity
@Table(name = "Instrument_Type")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InstrumentType.findAll", query = "SELECT i FROM InstrumentType i"),
    @NamedQuery(name = "InstrumentType.findByInstrumentTypeId", query = "SELECT i FROM InstrumentType i WHERE i.instrumentTypeId = :instrumentTypeId"),
    @NamedQuery(name = "InstrumentType.findByInstrumentTypeName", query = "SELECT i FROM InstrumentType i WHERE i.instrumentTypeName = :instrumentTypeName")})
public class InstrumentType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "instrument_type_id")
    private Integer instrumentTypeId;
    @Basic(optional = false)
    @Column(name = "instrument_type_name")
    private String instrumentTypeName;

    public InstrumentType() {
    }

    public InstrumentType(Integer instrumentTypeId) {
        this.instrumentTypeId = instrumentTypeId;
    }

    public InstrumentType(Integer instrumentTypeId, String instrumentTypeName) {
        this.instrumentTypeId = instrumentTypeId;
        this.instrumentTypeName = instrumentTypeName;
    }

    public Integer getInstrumentTypeId() {
        return instrumentTypeId;
    }

    public void setInstrumentTypeId(Integer instrumentTypeId) {
        this.instrumentTypeId = instrumentTypeId;
    }

    public String getInstrumentTypeName() {
        return instrumentTypeName;
    }

    public void setInstrumentTypeName(String instrumentTypeName) {
        this.instrumentTypeName = instrumentTypeName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (instrumentTypeId != null ? instrumentTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstrumentType)) {
            return false;
        }
        InstrumentType other = (InstrumentType) object;
        if ((this.instrumentTypeId == null && other.instrumentTypeId != null) || (this.instrumentTypeId != null && !this.instrumentTypeId.equals(other.instrumentTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.Granberg.model.InstrumentType[ instrumentTypeId=" + instrumentTypeId + " ]";
    }
    
}
