/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.controller.exceptions.NonexistentEntityException;
import com.granberg.model.Instrument;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class InstrumentJpaController implements Serializable {

    public InstrumentJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Instrument instrument) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(instrument);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Instrument instrument) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            instrument = em.merge(instrument);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = instrument.getInstrumentId();
                if (findInstrument(id) == null) {
                    throw new NonexistentEntityException("The instrument with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Instrument instrument;
            try {
                instrument = em.getReference(Instrument.class, id);
                instrument.getInstrumentId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The instrument with id " + id + " no longer exists.", enfe);
            }
            em.remove(instrument);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Instrument> findInstrumentEntities() {
        return findInstrumentEntities(true, -1, -1);
    }

    public List<Instrument> findInstrumentEntities(int maxResults, int firstResult) {
        return findInstrumentEntities(false, maxResults, firstResult);
    }

    private List<Instrument> findInstrumentEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Instrument.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Instrument findInstrument(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Instrument.class, id);
        } finally {
            em.close();
        }
    }

    public int getInstrumentCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Instrument> rt = cq.from(Instrument.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
