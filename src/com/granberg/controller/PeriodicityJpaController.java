/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg.controller;

import com.granberg.exceptions.NonexistentEntityException;
import com.granberg.model.Periodicity;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Carlo
 */
public class PeriodicityJpaController implements Serializable {

    public PeriodicityJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Periodicity periodicity) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(periodicity);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Periodicity periodicity) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            periodicity = em.merge(periodicity);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = periodicity.getPeriodicityId();
                if (findPeriodicity(id) == null) {
                    throw new NonexistentEntityException("The periodicity with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Periodicity periodicity;
            try {
                periodicity = em.getReference(Periodicity.class, id);
                periodicity.getPeriodicityId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The periodicity with id " + id + " no longer exists.", enfe);
            }
            em.remove(periodicity);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Periodicity> findPeriodicityEntities() {
        return findPeriodicityEntities(true, -1, -1);
    }

    public List<Periodicity> findPeriodicityEntities(int maxResults, int firstResult) {
        return findPeriodicityEntities(false, maxResults, firstResult);
    }

    private List<Periodicity> findPeriodicityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Periodicity.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Periodicity findPeriodicity(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Periodicity.class, id);
        } finally {
            em.close();
        }
    }

    public int getPeriodicityCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Periodicity> rt = cq.from(Periodicity.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
