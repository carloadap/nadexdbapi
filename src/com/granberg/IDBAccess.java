/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg;

import com.granberg.model.OrderType;
import com.granberg.model.Market;
import com.granberg.model.InstrumentType;
import com.granberg.model.Instrument;
import com.granberg.model.News;
import com.granberg.model.Contract;
import com.granberg.model.Periodicity;
import com.granberg.model.Position;
import com.granberg.model.Ticket;

import java.util.List;

/**
 *
 * @author Carlo
 */
public interface IDBAccess {
    List<Market> findMarket();
    List<Instrument> findInstrument();
    List<InstrumentType> findInstrumentType();
    List<OrderType> findOrderType();
    List<News> findNews();
    List<Contract> findContracts();
    List<Ticket> findTickets();
    List<Position> findPositions();
    List<Periodicity> findPeriodicity();
}
