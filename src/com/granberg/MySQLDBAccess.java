/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.granberg;

import com.granberg.controller.ContractJpaController;
import com.granberg.controller.InstrumentJpaController;
import com.granberg.controller.InstrumentTypeJpaController;
import com.granberg.controller.MarketJpaController;
import com.granberg.controller.NewsJpaController;
import com.granberg.controller.OrderTypeJpaController;
import com.granberg.controller.PeriodicityJpaController;
import com.granberg.controller.PositionJpaController;
import com.granberg.controller.TicketJpaController;
import com.granberg.model.Contract;
import com.granberg.model.Instrument;
import com.granberg.model.InstrumentType;
import com.granberg.model.Market;
import com.granberg.model.News;
import com.granberg.model.OrderType;
import com.granberg.model.Periodicity;
import com.granberg.model.Position;
import com.granberg.model.Ticket;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Carlo
 */
public class MySQLDBAccess implements IDBAccess {
    private EntityManagerFactory emf;
    public MySQLDBAccess(){
        emf = Persistence.createEntityManagerFactory("NadexDBAPIPU");
    }
    @Override
    public List<Market> findMarket() {
        
        MarketJpaController marketController = new MarketJpaController(emf);
        return marketController.findMarketEntities();
        
    }

    @Override
    public List<Instrument> findInstrument() {
        InstrumentJpaController instrumentController = new InstrumentJpaController(emf);
        return instrumentController.findInstrumentEntities();
    }

    @Override
    public List<InstrumentType> findInstrumentType() {
        InstrumentTypeJpaController instrumentTypeController = new InstrumentTypeJpaController(emf);
        return instrumentTypeController.findInstrumentTypeEntities();
    }

    @Override
    public List<OrderType> findOrderType() {
        OrderTypeJpaController orderTypeController = new OrderTypeJpaController(emf);
        return orderTypeController.findOrderTypeEntities();
    }

    @Override
    public List<News> findNews() {
        NewsJpaController newsController = new NewsJpaController(emf);
        return newsController.findNewsEntities();
    }

    @Override
    public List<Contract> findContracts() {
        ContractJpaController contractController = new ContractJpaController(emf);
        return contractController.findContractEntities();
    }

    @Override
    public List<Ticket> findTickets() {
        TicketJpaController ticketController = new TicketJpaController(emf);
        return ticketController.findTicketEntities();
    }

    @Override
    public List<Position> findPositions() {
        PositionJpaController positionController = new PositionJpaController(emf);
        return positionController.findPositionEntities();
    }

    @Override
    public List<Periodicity> findPeriodicity() {
        PeriodicityJpaController periodicityController = new PeriodicityJpaController(emf);
        return periodicityController.findPeriodicityEntities();
    }
    
}
